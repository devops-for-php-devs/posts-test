@setup
if (!$repo || !$servers) {
exit('The repo and servers parameter is required');
}
$servers = json_decode($servers, true, JSON_THROW_ON_ERROR);
if (!$servers || count($servers) < 1) {
exit('servers parameter should be a valid json array of servers.');
}
$persistentDir = '/var/www/app/persistent';
$releasesDir = '/var/www/app/releases';
$appDir = '/var/www/app/current';
$release = date('YmdHis');
$newReleaseDir = $releasesDir .'/'. $release;
$persistentStorageExists = is_dir($persistentDir . '/storage');
@endsetup

@servers($servers)

@story('deploy')
cloneRelease
linkPersistentStorage
installDependencies
optimiseRelease
activateRelease
@endstory

@task('cloneRelease')
echo 'Cloning repository into release {{ $release }}'

[ -d {{ $releasesDir }} ] || mkdir -p {{ $releasesDir }}
cd {{ $releasesDir }}
git clone -b master --depth 1 {{ $repo }} {{ $release }}

@if(!$persistentStorageExists)
    mkdir -p {{ $persistentDir }}/storage
    cp -rf {{ $newReleaseDir }}/storage {{ $persistentDir }}
@endif

rm -rf {{ $newReleaseDir }}/storage
@endtask

@task('linkPersistentStorage')
echo 'Link Persistent Storage'
ln -nfs {{ $persistentDir }}/.env {{ $newReleaseDir }}/.env
ln -nfs {{ $persistentDir }}/storage {{ $newReleaseDir }}/storage
@endtask

@task('installDependencies')
echo "Installing Dependencies"
cd {{ $newReleaseDir }}
composer install --no-dev --prefer-dist -q -o
@endtask

@task('optimiseRelease')
echo 'Linking public storage directory'

cd {{ $newReleaseDir }}
php artisan storage:link

echo 'Migrating Database'
php artisan migrate --force

echo 'Caching routes and configs'
php artisan route:cache
php artisan config:cache
@endtask

@task('activateRelease')
echo 'Linking current release'
rm -rf {{ $appDir }}
ln -nfs {{ $newReleaseDir }} {{ $appDir }}

sudo service php7.4-fpm restart

{{-- echo 'Restarting Horizon' --}}
{{-- php artisan horizon:terminate --}}

echo 'Removing old releases'
cd {{ $releasesDir }}
echo 'Current Releases:'
ls
echo 'Releases to remove:'
ls | head -n -6

if (($(ls | head -n -6 | wc -l) > 0)); then rm -rf $(ls | head -n -6); fi
@endtask
