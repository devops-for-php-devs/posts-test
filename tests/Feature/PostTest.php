<?php

namespace Tests\Feature;

use App\Models\Post;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PostTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @return void
     */
    public function testCanCreatePost(): void
    {
        $post = Post::factory()->raw();

        $this->assertEquals(0, Post::count());

        $this->withExceptionHandling()
            ->post(route('posts.store'), [])
            ->assertSessionHasErrors(['content']);

        $this->assertEquals(0, Post::count());

        $this->withoutExceptionHandling()
            ->followingRedirects()
            ->post(route('posts.store'), $post)
            ->assertSuccessful();

        $this->assertDatabaseHas('posts', ['content' => $post['content']]);
    }

    /**
     * @return void
     */
    public function testCanEditPosts(): void
    {
        $post = Post::factory()->create();

        $this->withExceptionHandling()
            ->put(route('posts.update', ['post' => $post]), [])
            ->assertSessionHasErrors(['content']);

        $this->assertDatabaseHas('posts', [
            'id' => $post->id,
            'content' => $post->content,
        ]);

        $postUpdate = Post::factory()->raw();

        $this->withoutExceptionHandling()
            ->followingRedirects()
            ->put(route('posts.update', ['post' => $post]), $postUpdate)
            ->assertSuccessful();

        $this->assertDatabaseHas('posts', [
            'id' => $post->id,
            'content' => $postUpdate['content'],
        ]);
    }

    /**
     * @return void
     */
    public function testCanDeletePosts(): void
    {
        $post = Post::factory()->create();

        $this->withoutExceptionHandling()
            ->followingRedirects()
            ->delete(route('posts.destroy', ['post' => $post]))
            ->assertSuccessful();

        $this->assertDatabaseMissing('posts', [
            'id' => $post->id,
            'content' => $post->content,
        ]);
    }
}
